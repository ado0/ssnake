#include "main.h"
#include "stm32f4xx_hal.h"
#include <stdio.h>
#include <string.h>
#include <time.h>

SPI_HandleTypeDef hspi3;
TIM_HandleTypeDef htim3;

void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_SPI3_Init(void);
static void MX_TIM3_Init(void);

#define READ(PIN) HAL_GPIO_ReadPin(PIN##_GPIO_Port, PIN##_Pin)
#define SET(PIN) HAL_GPIO_WritePin(PIN##_GPIO_Port, PIN##_Pin, SET)
#define RESET(PIN) HAL_GPIO_WritePin(PIN##_GPIO_Port, PIN##_Pin, RESET)
#define TOGGLE(PIN) HAL_GPIO_TogglePin(PIN##_GPIO_Port, PIN##_Pin)
#define SLEEP(TIME) HAL_Delay(TIME)

#define DISPLAY_W 84
#define DISPLAY_H 48

#define DATA_H 8

#define IMG_SIZE DISPLAY_W *DISPLAY_H / DATA_H

static const uint8_t startup_img[IMG_SIZE] = {
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x80,
    0xe0, 0xe0, 0xe0, 0x60, 0x00, 0x00, 0x00, 0x00, 0xe0, 0xe0, 0xe0, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0xe0, 0xe0, 0xe0, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0xc0, 0xe0, 0xe0, 0xe0, 0xe0, 0xc0, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0xe0, 0xe0, 0xe0, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0xe0, 0xe0, 0xe0, 0x00, 0x00, 0x00, 0x00, 0xe0, 0xe0, 0xe0, 0xe0,
    0xe0, 0xe0, 0xe0, 0xe0, 0xe0, 0xe0, 0xe0, 0xe0, 0x00, 0x00, 0x00, 0x00,

    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x80, 0xe0, 0xf8, 0xfe, 0xff,
    0x9f, 0x87, 0x81, 0x00, 0x00, 0x00, 0x00, 0x00, 0xff, 0xff, 0xff, 0xf8,
    0xe0, 0x80, 0x00, 0x00, 0x00, 0xff, 0xff, 0xff, 0x00, 0x00, 0x00, 0x00,
    0xf0, 0xfc, 0xff, 0xcf, 0xc3, 0xc0, 0xc0, 0xc3, 0xcf, 0xff, 0xfc, 0xf0,
    0x00, 0x00, 0x00, 0x00, 0xff, 0xff, 0xff, 0xc0, 0xf0, 0x70, 0x1c, 0x1c,
    0x07, 0x07, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0xff, 0xff, 0xff, 0x80,
    0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,

    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x81, 0xe1, 0xf9, 0xff, 0x7f,
    0x1f, 0x07, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0xff, 0xff, 0xff, 0x00,
    0x03, 0x0f, 0x3e, 0xf8, 0xe0, 0xff, 0xff, 0xff, 0x00, 0x00, 0x00, 0x00,
    0xff, 0xff, 0xff, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0xff, 0xff, 0xff,
    0x00, 0x00, 0x00, 0x00, 0xff, 0xff, 0xff, 0x03, 0x0f, 0x0e, 0x38, 0x38,
    0xe0, 0xe0, 0x80, 0x80, 0x00, 0x00, 0x00, 0x00, 0xff, 0xff, 0xff, 0x03,
    0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,

    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x06, 0x07, 0x07, 0x07, 0x01, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x07, 0x07, 0x07, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x03, 0x07, 0x07, 0x07, 0x00, 0x00, 0x00, 0x00,
    0x07, 0x07, 0x07, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x07, 0x07, 0x07,
    0x00, 0x00, 0x00, 0x00, 0x07, 0x07, 0x07, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x07, 0x07, 0x07, 0x00, 0x00, 0x00, 0x00, 0x07, 0x07, 0x07, 0x07,
    0x07, 0x07, 0x07, 0x07, 0x07, 0x07, 0x07, 0x07, 0x00, 0x00, 0x00, 0x00,

    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00,

    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00,
};

static void display_write_data_array(const uint8_t *data_array,
                                     uint32_t data_size)
{
        SET(DC);

        RESET(CE);
        HAL_SPI_Transmit(&hspi3, data_array, data_size, HAL_MAX_DELAY);
        SET(CE);
}

static void display_write_data(uint8_t data) // DC high -> write data (D/C)
{
        display_write_data_array(data, 1);
}

static void display_write_code_array(const uint8_t *code_array,
                                     uint32_t code_size)
{
        RESET(DC);

        RESET(CE);
        HAL_SPI_Transmit(&hspi3, code_array, code_size, HAL_MAX_DELAY);
        SET(CE);
}

static void display_write_code(uint8_t code) // DC low -> write code (D/C)
{
        display_write_code_array(code, 1);
}

/*
 *
 * 0x01xxxxxxx -> set x command
 * 0x001xxxxxx -> set y command
 *
 */

static void display_set_x(uint8_t x) { display_write_code(128 | x); }

static void display_set_y(uint8_t y) { display_write_code(64 | y); }

static void display_set_xy(uint8_t x, uint8_t y)
{
        display_write_code_array((const uint8_t[]){128 | x, 64 | y}, 2);
}

/*
 *
 * 0x0d -> invert colors
 * 0x0c -> normal colors
 * 0x21 -> set extended commands
 * 0xb9 -> set contrast
 * 0x04 -> set temperature coeficient
 * 0x14 -> set bias mode
 * 0x20 -> set basic commands
 * 0x0c -> set normal mode
 *
 */

static void display_init()
{
        RESET(RST);
        SET(RST);

        uint8_t code_array[] = {0x0c, 0x23, 0xb9, 0x14, 0x04, 0x20};
        display_write_code_array(code_array, 6);
}

static void display_print_img(const uint8_t img[DISPLAY_W * IMG_SIZE])
{
        display_write_data_array(img, IMG_SIZE);
}

#define LETTER_W 6
#define LETTER_H 8

typedef uint8_t letter_t[LETTER_W];

const static letter_t letters[] = {
    {0x00, 0x00, 0x00, 0x00, 0x00, 0x00} // 20
    ,
    {0x00, 0x00, 0x00, 0x5f, 0x00, 0x00} // 21 !
    ,
    {0x00, 0x00, 0x07, 0x00, 0x07, 0x00} // 22 "
    ,
    {0x00, 0x14, 0x7f, 0x14, 0x7f, 0x14} // 23 #
    ,
    {0x00, 0x24, 0x2a, 0x7f, 0x2a, 0x12} // 24 $
    ,
    {0x00, 0x23, 0x13, 0x08, 0x64, 0x62} // 25 %
    ,
    {0x00, 0x36, 0x49, 0x55, 0x22, 0x50} // 26 &
    ,
    {0x00, 0x00, 0x05, 0x03, 0x00, 0x00} // 27 '
    ,
    {0x00, 0x00, 0x1c, 0x22, 0x41, 0x00} // 28 (
    ,
    {0x00, 0x00, 0x41, 0x22, 0x1c, 0x00} // 29 )
    ,
    {0x00, 0x14, 0x08, 0x3e, 0x08, 0x14} // 2a *
    ,
    {0x00, 0x08, 0x08, 0x3e, 0x08, 0x08} // 2b +
    ,
    {0x00, 0x00, 0x50, 0x30, 0x00, 0x00} // 2c ,
    ,
    {0x00, 0x08, 0x08, 0x08, 0x08, 0x08} // 2d -
    ,
    {0x00, 0x00, 0x60, 0x60, 0x00, 0x00} // 2e .
    ,
    {0x00, 0x20, 0x10, 0x08, 0x04, 0x02} // 2f /
    ,
    {0x00, 0x3e, 0x51, 0x49, 0x45, 0x3e} // 30 0
    ,
    {0x00, 0x00, 0x42, 0x7f, 0x40, 0x00} // 31 1
    ,
    {0x00, 0x42, 0x61, 0x51, 0x49, 0x46} // 32 2
    ,
    {0x00, 0x21, 0x41, 0x45, 0x4b, 0x31} // 33 3
    ,
    {0x00, 0x18, 0x14, 0x12, 0x7f, 0x10} // 34 4
    ,
    {0x00, 0x27, 0x45, 0x45, 0x45, 0x39} // 35 5
    ,
    {0x00, 0x3c, 0x4a, 0x49, 0x49, 0x30} // 36 6
    ,
    {0x00, 0x01, 0x71, 0x09, 0x05, 0x03} // 37 7
    ,
    {0x00, 0x36, 0x49, 0x49, 0x49, 0x36} // 38 8
    ,
    {0x00, 0x06, 0x49, 0x49, 0x29, 0x1e} // 39 9
    ,
    {0x00, 0x00, 0x36, 0x36, 0x00, 0x00} // 3a :
    ,
    {0x00, 0x00, 0x56, 0x36, 0x00, 0x00} // 3b ;
    ,
    {0x00, 0x08, 0x14, 0x22, 0x41, 0x00} // 3c <
    ,
    {0x00, 0x14, 0x14, 0x14, 0x14, 0x14} // 3d =
    ,
    {0x00, 0x00, 0x41, 0x22, 0x14, 0x08} // 3e >
    ,
    {0x00, 0x02, 0x01, 0x51, 0x09, 0x06} // 3f ?
    ,
    {0x00, 0x32, 0x49, 0x79, 0x41, 0x3e} // 40 @
    ,
    {0x00, 0x7e, 0x11, 0x11, 0x11, 0x7e} // 41 A
    ,
    {0x00, 0x7f, 0x49, 0x49, 0x49, 0x36} // 42 B
    ,
    {0x00, 0x3e, 0x41, 0x41, 0x41, 0x22} // 43 C
    ,
    {0x00, 0x7f, 0x41, 0x41, 0x22, 0x1c} // 44 D
    ,
    {0x00, 0x7f, 0x49, 0x49, 0x49, 0x41} // 45 E
    ,
    {0x00, 0x7f, 0x09, 0x09, 0x09, 0x01} // 46 F
    ,
    {0x00, 0x3e, 0x41, 0x49, 0x49, 0x7a} // 47 G
    ,
    {0x00, 0x7f, 0x08, 0x08, 0x08, 0x7f} // 48 H
    ,
    {0x00, 0x00, 0x41, 0x7f, 0x41, 0x00} // 49 I
    ,
    {0x00, 0x20, 0x40, 0x41, 0x3f, 0x01} // 4a J
    ,
    {0x00, 0x7f, 0x08, 0x14, 0x22, 0x41} // 4b K
    ,
    {0x00, 0x7f, 0x40, 0x40, 0x40, 0x40} // 4c L
    ,
    {0x00, 0x7f, 0x02, 0x0c, 0x02, 0x7f} // 4d M
    ,
    {0x00, 0x7f, 0x04, 0x08, 0x10, 0x7f} // 4e N
    ,
    {0x00, 0x3e, 0x41, 0x41, 0x41, 0x3e} // 4f O
    ,
    {0x00, 0x7f, 0x09, 0x09, 0x09, 0x06} // 50 P
    ,
    {0x00, 0x3e, 0x41, 0x51, 0x21, 0x5e} // 51 Q
    ,
    {0x00, 0x7f, 0x09, 0x19, 0x29, 0x46} // 52 R
    ,
    {0x00, 0x46, 0x49, 0x49, 0x49, 0x31} // 53 S
    ,
    {0x00, 0x01, 0x01, 0x7f, 0x01, 0x01} // 54 T
    ,
    {0x00, 0x3f, 0x40, 0x40, 0x40, 0x3f} // 55 U
    ,
    {0x00, 0x1f, 0x20, 0x40, 0x20, 0x1f} // 56 V
    ,
    {0x00, 0x3f, 0x40, 0x38, 0x40, 0x3f} // 57 W
    ,
    {0x00, 0x63, 0x14, 0x08, 0x14, 0x63} // 58 X
    ,
    {0x00, 0x07, 0x08, 0x70, 0x08, 0x07} // 59 Y
    ,
    {0x00, 0x61, 0x51, 0x49, 0x45, 0x43} // 5a Z
    ,
    {0x00, 0x00, 0x7f, 0x41, 0x41, 0x00} // 5b [
    ,
    {0x00, 0x02, 0x04, 0x08, 0x10, 0x20} // 5c Â¥
    ,
    {0x00, 0x00, 0x41, 0x41, 0x7f, 0x00} // 5d ]
    ,
    {0x00, 0x04, 0x02, 0x01, 0x02, 0x04} // 5e ^
    ,
    {0x00, 0x00, 0x00, 0x00, 0x00, 0x00} // 5f _
    ,
    {0x00, 0x00, 0x01, 0x02, 0x04, 0x00} // 60 `
    ,
    {0x00, 0x20, 0x54, 0x54, 0x54, 0x78} // 61 a
    ,
    {0x00, 0x7f, 0x48, 0x44, 0x44, 0x38} // 62 b
    ,
    {0x00, 0x38, 0x44, 0x44, 0x44, 0x20} // 63 c
    ,
    {0x00, 0x38, 0x44, 0x44, 0x48, 0x7f} // 64 d
    ,
    {0x00, 0x38, 0x54, 0x54, 0x54, 0x18} // 65 e
    ,
    {0x00, 0x08, 0x7e, 0x09, 0x01, 0x02} // 66 f
    ,
    {0x00, 0x0c, 0x52, 0x52, 0x52, 0x3e} // 67 g
    ,
    {0x00, 0x7f, 0x08, 0x04, 0x04, 0x78} // 68 h
    ,
    {0x00, 0x00, 0x44, 0x7d, 0x40, 0x00} // 69 i
    ,
    {0x00, 0x20, 0x40, 0x44, 0x3d, 0x00} // 6a j
    ,
    {0x00, 0x7f, 0x10, 0x28, 0x44, 0x00} // 6b k
    ,
    {0x00, 0x00, 0x41, 0x7f, 0x40, 0x00} // 6c l
    ,
    {0x00, 0x7c, 0x04, 0x18, 0x04, 0x78} // 6d m
    ,
    {0x00, 0x7c, 0x08, 0x04, 0x04, 0x78} // 6e n
    ,
    {0x00, 0x38, 0x44, 0x44, 0x44, 0x38} // 6f o
    ,
    {0x00, 0x7c, 0x14, 0x14, 0x14, 0x08} // 70 p
    ,
    {0x00, 0x08, 0x14, 0x14, 0x18, 0x7c} // 71 q
    ,
    {0x00, 0x7c, 0x08, 0x04, 0x04, 0x08} // 72 r
    ,
    {0x00, 0x48, 0x54, 0x54, 0x54, 0x20} // 73 s
    ,
    {0x00, 0x04, 0x3f, 0x44, 0x40, 0x20} // 74 t
    ,
    {0x00, 0x3c, 0x40, 0x40, 0x20, 0x7c} // 75 u
    ,
    {0x00, 0x1c, 0x20, 0x40, 0x20, 0x1c} // 76 v
    ,
    {0x00, 0x3c, 0x40, 0x30, 0x40, 0x3c} // 77 w
    ,
    {0x00, 0x44, 0x28, 0x10, 0x28, 0x44} // 78 x
    ,
    {0x00, 0x0c, 0x50, 0x50, 0x50, 0x3c} // 79 y
    ,
    {0x00, 0x44, 0x64, 0x54, 0x4c, 0x44} // 7a z
    ,
    {0x00, 0x00, 0x08, 0x36, 0x41, 0x00} // 7b {
    ,
    {0x00, 0x00, 0x00, 0x7f, 0x00, 0x00} // 7c |
    ,
    {0x00, 0x00, 0x41, 0x36, 0x08, 0x00} // 7d }
    ,
    {0x00, 0x10, 0x08, 0x08, 0x10, 0x08} // 7e â†�
    ,
    {0x00, 0x78, 0x46, 0x41, 0x46, 0x78} // 7f â†’
};

static letter_t *text_get_letter(char c) { return &letters[c - ' ']; }

static void text_print(const char *word, uint8_t y, uint8_t x)
{
        display_set_xy(x * LETTER_W, y);
        for(size_t i = 0; i < strlen(word); ++i, ++x)
        {
                display_write_data_array(text_get_letter(word[i]), LETTER_W);
        }
}

#define SPRITE_W 8
#define EDGE_W 2

#define SPRITE_NUM_W 10
#define SPRITE_NUM_H 6

typedef uint8_t sprite_t[SPRITE_W];

#define HEAD_U 0
#define HEAD_R 1
#define HEAD_D 2
#define HEAD_L 3

#define TAIL_U 4
#define TAIL_R 5
#define TAIL_D 6
#define TAIL_L 7

#define BODY_H 8
#define BODY_R 8
#define BODY_L 8

#define BODY_V 9
#define BODY_U 9
#define BODY_D 9

#define BODY_UR 10
#define BODY_RD 11
#define BODY_DL 12
#define BODY_LU 13

#define NONE 14
#define FILL 15
#define FOOD 16

#define FILL_EDGE 17

const static sprite_t snake_sprites[] = {
    {0xfc, 0xfe, 0xff, 0xff, 0xff, 0xf7, 0xfe, 0xfc} // head up
    ,
    {0xff, 0xff, 0xff, 0xff, 0xff, 0xfb, 0x7e, 0x3c} // head right
    ,
    {0x3f, 0x7f, 0xff, 0xff, 0xff, 0xef, 0x7f, 0x3f} // head down
    ,
    {0x3c, 0x7e, 0xfb, 0xff, 0xff, 0xff, 0xff, 0xff} // head left
    ,
    {0x00, 0xe0, 0xfc, 0xfe, 0xfe, 0xfc, 0xe0, 0x00} // tail up
    ,
    {0x7e, 0x7e, 0x7e, 0x3c, 0x3c, 0x3c, 0x18, 0x00} // tail right
    ,
    {0x00, 0x07, 0x3f, 0x7f, 0x7f, 0x3f, 0x07, 0x00} // tail down
    ,
    {0x00, 0x18, 0x3c, 0x3c, 0x3c, 0x7e, 0x7e, 0x7e} // tail left
    ,
    {0x7e, 0x7e, 0x7e, 0x7e, 0x7e, 0x7e, 0x7e, 0x7e} // body horisontal
    ,
    {0x00, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0x00} // body vertical
    ,
    {0x00, 0xfc, 0xfe, 0xfe, 0xfe, 0xfe, 0xfe, 0x7e} // body up-right
    ,
    {0x7e, 0xfe, 0xfe, 0xfe, 0xfe, 0xfe, 0xfc, 0x00} // body right-down
    ,
    {0x7e, 0x7f, 0x7f, 0x7f, 0x7f, 0x7f, 0x3f, 0x00} // body down-left
    ,
    {0x00, 0x3f, 0x7f, 0x7f, 0x7f, 0x7f, 0x7f, 0x7e} // body left-up
    ,
    {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00} // none
    ,
    {0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff} // fill
    ,
    {0x7e, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0x7e} // fill edge
    ,
    {0x00, 0x24, 0x5a, 0x3c, 0x3c, 0x5a, 0x24, 0x00} // food
};

static sprite_t *snake_img[SPRITE_NUM_H][SPRITE_NUM_W];

static void snake_print()
{
        display_set_xy(0, 0);
        for(size_t i = 0; i < SPRITE_NUM_H; ++i)
        {
                display_write_data_array((const uint8_t[]){0x77, 0xaa}, 2);
                for(size_t j = 0; j < SPRITE_NUM_W; ++j)
                {
                        display_write_data_array(*snake_img[i][j], 8);
                }
                display_write_data_array((const uint8_t[]){0x77, 0xaa}, 2);
        }
}

static void snake_clear()
{
        for(size_t i = 0; i < SPRITE_NUM_H; ++i)
        {
                for(size_t j = 0; j < SPRITE_NUM_W; ++j)
                {
                        snake_img[i][j] = &snake_sprites[NONE];
                }
        }
}

static void snake_set(const sprite_t *sprite, uint8_t y, uint8_t x)
{
        snake_img[y][x] = sprite;
        display_set_xy(x * SPRITE_W + EDGE_W, y);
        display_write_data_array(*sprite, sizeof(uint8_t) * SPRITE_W);
}

static void snake_place_food()
{
        uint8_t x, y;
        while(1)
        {
                y = rand() % 6;
                x = rand() % 10;
                if(snake_img[y][x] == &snake_sprites[NONE])
                {
                        snake_set(&snake_sprites[FOOD], y, x);
                        break;
                }
        }
}

struct segment
{
        sprite_t *s;
        uint8_t y;
        uint8_t x;
};

typedef struct segment segment_t;

#define UP 0
#define RIGHT 1
#define DOWN 2
#define LEFT 3

#define ALIVE 0
#define DEAD 1
#define MENU 2
#define INIT_IMG 3
#define PAUSE 4

#define FALSE 0
#define TRUE 1

struct queue
{
        uint8_t size;
        uint8_t max;
        uint8_t front;
        uint8_t back;
        uint8_t direction;
        uint8_t status;
        uint8_t score;
        uint8_t direction_set;
        uint8_t speed;
        segment_t data[SPRITE_NUM_H * SPRITE_NUM_W];
};

static struct queue snake;

#define QUEUE_BACK snake.data[snake.back]
#define QUEUE_FRONT snake.data[snake.front]

static void queue_init(void)
{
        snake.size = 3;
        snake.max = SPRITE_NUM_H * SPRITE_NUM_W;
        snake.front = 0;
        snake.back = 2;
        snake.direction = RIGHT;
        snake.status = ALIVE;
        snake.direction_set = TRUE;
        snake.score = 0;

        snake.data[0] = (segment_t){&snake_sprites[TAIL_L], 0, 0};
        snake.data[1] = (segment_t){&snake_sprites[BODY_H], 0, 1};
        snake.data[2] = (segment_t){&snake_sprites[HEAD_R], 0, 2};

        snake_set(snake.data[0].s, snake.data[0].y, snake.data[0].x);
        snake_set(snake.data[1].s, snake.data[1].y, snake.data[1].x);
        snake_set(snake.data[2].s, snake.data[2].y, snake.data[2].x);
}

static void queue_next(uint8_t *curr)
{
        *curr = (*curr + 1 == snake.max) ? 0 : *curr + 1;
}

static void queue_pop_up(void)
{
        if(QUEUE_FRONT.s == &snake_sprites[BODY_V])
        {
                QUEUE_FRONT.s = &snake_sprites[TAIL_U];
        }
        else if(QUEUE_FRONT.s == &snake_sprites[BODY_LU])
        {
                QUEUE_FRONT.s = &snake_sprites[TAIL_L];
        }
        else if(QUEUE_FRONT.s == &snake_sprites[BODY_DL])
        {
                QUEUE_FRONT.s = &snake_sprites[TAIL_R];
        }
}

static void queue_pop_right(void)
{
        if(QUEUE_FRONT.s == &snake_sprites[BODY_H])
        {
                QUEUE_FRONT.s = &snake_sprites[TAIL_R];
        }
        else if(QUEUE_FRONT.s == &snake_sprites[BODY_LU])
        {
                QUEUE_FRONT.s = &snake_sprites[TAIL_D];
        }
        else if(QUEUE_FRONT.s == &snake_sprites[BODY_UR])
        {
                QUEUE_FRONT.s = &snake_sprites[TAIL_U];
        }
}

static void queue_pop_down(void)
{
        if(QUEUE_FRONT.s == &snake_sprites[BODY_V])
        {
                QUEUE_FRONT.s = &snake_sprites[TAIL_D];
        }
        else if(QUEUE_FRONT.s == &snake_sprites[BODY_UR])
        {
                QUEUE_FRONT.s = &snake_sprites[TAIL_L];
        }
        else if(QUEUE_FRONT.s == &snake_sprites[BODY_RD])
        {
                QUEUE_FRONT.s = &snake_sprites[TAIL_R];
        }
}

static void queue_pop_left(void)
{
        if(QUEUE_FRONT.s == &snake_sprites[BODY_H])
        {
                QUEUE_FRONT.s = &snake_sprites[TAIL_L];
        }
        else if(QUEUE_FRONT.s == &snake_sprites[BODY_DL])
        {
                QUEUE_FRONT.s = &snake_sprites[TAIL_D];
        }
        else if(QUEUE_FRONT.s == &snake_sprites[BODY_RD])
        {
                QUEUE_FRONT.s = &snake_sprites[TAIL_U];
        }
}

static void queue_pop(void)
{
        uint8_t tail_direction = 0;
        if(QUEUE_FRONT.s == &snake_sprites[TAIL_U])
        {
                tail_direction = TAIL_U;
        }
        else if(QUEUE_FRONT.s == &snake_sprites[TAIL_R])
        {
                tail_direction = TAIL_R;
        }
        else if(QUEUE_FRONT.s == &snake_sprites[TAIL_D])
        {
                tail_direction = TAIL_D;
        }
        else if(QUEUE_FRONT.s == &snake_sprites[TAIL_L])
        {
                tail_direction = TAIL_L;
        }

        QUEUE_FRONT.s = snake_sprites[NONE];
        snake_set(snake_sprites[NONE], snake.data[snake.front].y,
                  snake.data[snake.front].x);
        queue_next(&snake.front);

        switch(tail_direction)
        {
        case TAIL_U:
                queue_pop_up();
                break;
        case TAIL_R:
                queue_pop_right();
                break;
        case TAIL_D:
                queue_pop_down();
                break;
        case TAIL_L:
                queue_pop_left();
                break;
        }

        snake_set(QUEUE_FRONT.s, QUEUE_FRONT.y, QUEUE_FRONT.x);
        --snake.size;
}

static void queue_push_up(void)
{
        if(QUEUE_BACK.y == 0)
        {
                snake.status = DEAD;
        }
        else
        {
                if(QUEUE_BACK.s == &snake_sprites[HEAD_U])
                {
                        QUEUE_BACK.s = &snake_sprites[BODY_V];
                }
                else if(QUEUE_BACK.s == &snake_sprites[HEAD_R])
                {
                        QUEUE_BACK.s = &snake_sprites[BODY_DL];
                }
                else
                {
                        QUEUE_BACK.s = &snake_sprites[BODY_LU];
                }
        }
}

static void queue_push_right(void)
{
        if(QUEUE_BACK.x == SPRITE_NUM_W - 1)
        {
                snake.status = DEAD;
        }
        else
        {
                if(QUEUE_BACK.s == &snake_sprites[HEAD_U])
                {
                        QUEUE_BACK.s = &snake_sprites[BODY_UR];
                }
                else if(QUEUE_BACK.s == &snake_sprites[HEAD_R])
                {
                        QUEUE_BACK.s = &snake_sprites[BODY_H];
                }
                else
                {
                        QUEUE_BACK.s = &snake_sprites[BODY_LU];
                }
        }
}

static void queue_push_down(void)
{
        if(QUEUE_BACK.y == SPRITE_NUM_H - 1)
        {
                snake.status = DEAD;
        }
        else
        {
                if(QUEUE_BACK.s == &snake_sprites[HEAD_R])
                {
                        QUEUE_BACK.s = &snake_sprites[BODY_RD];
                }
                else if(QUEUE_BACK.s == &snake_sprites[HEAD_D])
                {
                        QUEUE_BACK.s = &snake_sprites[BODY_V];
                }
                else
                {
                        QUEUE_BACK.s = &snake_sprites[BODY_UR];
                }
        }
}

static void queue_push_left(void)
{
        if(QUEUE_BACK.x == 0)
        {
                snake.status = DEAD;
        }
        else
        {
                if(QUEUE_BACK.s == &snake_sprites[HEAD_U])
                {
                        QUEUE_BACK.s = &snake_sprites[BODY_RD];
                }
                else if(QUEUE_BACK.s == &snake_sprites[HEAD_D])
                {
                        QUEUE_BACK.s = &snake_sprites[BODY_DL];
                }
                else
                {
                        QUEUE_BACK.s = &snake_sprites[BODY_H];
                }
        }
}

static void queue_push(void)
{
        uint8_t x = QUEUE_BACK.x;
        uint8_t y = QUEUE_BACK.y;

        uint8_t new_x = x;
        uint8_t new_y = y;
        sprite_t *new_s;

        switch(snake.direction)
        {
        case UP:
                queue_push_up();
                --new_y;
                new_s = &snake_sprites[HEAD_U];
                break;
        case RIGHT:
                queue_push_right();
                ++new_x;
                new_s = &snake_sprites[HEAD_R];
                break;
        case DOWN:
                queue_push_down();
                ++new_y;
                new_s = &snake_sprites[HEAD_D];
                break;
        case LEFT:
                queue_push_left();
                --new_x;
                new_s = &snake_sprites[HEAD_L];
                break;
        }

        if(snake_img[new_y][new_x] != &snake_sprites[NONE] &&
           snake_img[new_y][new_x] != &snake_sprites[FOOD] &&
           snake_img[new_y][new_x] != &snake_sprites[TAIL_U] &&
           snake_img[new_y][new_x] != &snake_sprites[TAIL_R] &&
           snake_img[new_y][new_x] != &snake_sprites[TAIL_D] &&
           snake_img[new_y][new_x] != &snake_sprites[TAIL_L])
        {
                snake.status = DEAD;
        }

        if(snake.status == ALIVE)
        {
                if(snake_img[new_y][new_x] != &snake_sprites[FOOD])
                {
                        queue_pop();
                }
                else
                {
                        ++snake.score;
                        snake_place_food();
                }

                snake_set(QUEUE_BACK.s, QUEUE_BACK.y, QUEUE_BACK.x);

                queue_next(&snake.back);
                QUEUE_BACK.s = new_s;
                QUEUE_BACK.x = new_x;
                QUEUE_BACK.y = new_y;

                snake_set(QUEUE_BACK.s, QUEUE_BACK.y, QUEUE_BACK.x);
                ++snake.size;
        }
}

// input
#define __D 0
#define __HASH 1
#define __0 2
#define __STAR 3
#define __C 4
#define __9 5
#define __8 6
#define __7 7
#define __B 8
#define __6 9
#define __5 10
#define __4 11
#define __A 12
#define __3 13
#define __2 14
#define __1 15

static void read_columns(uint8_t *col)
{
        col[0] = READ(D2);
        col[1] = READ(D3);
        col[2] = READ(D4);
        col[3] = READ(D5);
}

static uint8_t read_row(uint32_t *port, uint32_t pin)
{
        uint8_t row = 0;

        SET(D9);
        SET(D8);
        SET(D7);

        row = (HAL_GPIO_ReadPin(port, pin) == GPIO_PIN_RESET) ? 1 : row;
        SET(D6);

        RESET(D7);
        row = (HAL_GPIO_ReadPin(port, pin) == GPIO_PIN_RESET) ? 2 : row;
        SET(D7);

        RESET(D8);
        row = (HAL_GPIO_ReadPin(port, pin) == GPIO_PIN_RESET) ? 3 : row;
        SET(D8);

        RESET(D9);
        row = (HAL_GPIO_ReadPin(port, pin) == GPIO_PIN_RESET) ? 4 : row;

        RESET(D8);
        RESET(D7);
        RESET(D6);

        return row;
}

static int read_input()
{
        uint8_t col[4];
        int button = -1;

        read_columns(col);
        if(col[0] == GPIO_PIN_RESET) // D2 -> A B C D
        {
                button = 0 + 4 * read_row(D2_GPIO_Port, D2_Pin);
        }
        else if(col[1] == GPIO_PIN_RESET) // D3 -> 3 6 9 #
        {
                button = 1 + 4 * read_row(D3_GPIO_Port, D3_Pin);
        }
        else if(col[2] == GPIO_PIN_RESET) // D4 -> 2 5 8 0
        {
                button = 2 + 4 * read_row(D4_GPIO_Port, D4_Pin);
        }
        else if(col[3] == GPIO_PIN_RESET) // D5 -> 1 4 7 *
        {
                button = 3 + 4 * read_row(D5_GPIO_Port, D5_Pin);
        }

        return button - 4;
}

static uint8_t is_any_pressed(void)
{
        uint8_t pressed = 0;
        int input = read_input();
        if(input >= 0 && input <= 15)
        {
                pressed = 1;
        }
        return pressed;
}

static void press_any_to_continue(uint32_t delay)
{
        SLEEP(delay);
        while(!is_any_pressed())
                ;
        SLEEP(delay);
}

static void wait_for_next_input(void)
{
        while(snake.direction_set == TRUE)
        {
                SLEEP(10);
        }
}

// game
static void update(void)
{
        int input = read_input();
        if(input == __7 && snake.direction != LEFT && snake.direction != RIGHT)
        {
                snake.direction = RIGHT;
                snake.direction_set = TRUE;
        }
        else if(input == __0 && snake.direction != DOWN &&
                snake.direction != UP)
        {
                snake.direction = UP;
                snake.direction_set = TRUE;
        }
        else if(input == __5 && snake.direction != UP &&
                snake.direction != DOWN)
        {
                snake.direction = DOWN;
                snake.direction_set = TRUE;
        }
        else if(input == __9 && snake.direction != RIGHT &&
                snake.direction != LEFT)
        {
                snake.direction = LEFT;
                snake.direction_set = TRUE;
        }
	else if(input == __D)
	{
		snake.status = PAUSE;
		while(is_any_pressed())
		{
			SLEEP(10);
		}
		while(1)
		{
			input = read_input();
			if(input == __A)
			{
				snake.status = ALIVE;
				break;
			}
		}
	}
}

static void init_peripherals(void)
{
        HAL_Init();
        SystemClock_Config();
        MX_GPIO_Init();
        MX_SPI3_Init();
        MX_TIM3_Init();
        HAL_TIM_Base_Start_IT(&htim3);
}

static void run_startup(void)
{
	int rand_num = 0;
        display_init();

        display_print_img(startup_img);
        snake.status = INIT_IMG;

        text_print("press any key!", 5, 0);
        press_any_to_continue(0);

	while(is_any_pressed() == TRUE)
	{
		++rand_num;
	}
	srand(rand_num);
}

static void run_menu(void)
{
        snake_clear();
        snake_print();

        text_print("SELECT", 0, 1);
        text_print("DIFFICULTY:", 1, 1);
        text_print("  easy", 3, 4);
        text_print(" normal", 4, 4);
        text_print("  hard", 5, 4);

        uint8_t selected = 3;
        while(1)
        {
                text_print(">>", (selected + 0) % 3 + 3, 2);
                text_print("  ", (selected + 1) % 3 + 3, 2);
                text_print("  ", (selected + 2) % 3 + 3, 2);

		int i = read_input();
		if(i == __0)
		{
			selected = (selected == 3) ? 5 : selected - 1;
		}
		else if(i == __5)
		{
			selected = (selected == 5) ? 3 : selected + 1;
		}
		else if(i == __8)
		{
			break;
		}
		while(is_any_pressed())
		{
			SLEEP(10);
		}
        }
	snake.speed = selected - 2;
}

static void snake_init(void)
{
        snake_clear();
        snake_print();
        snake_place_food();
        queue_init();
}

static void run_end_screen(void)
{
        char buff[3];
        snake_clear();
        snake_print();

        text_print("YOU DIED", 0, 2);
        text_print("SCORE: ", 1, 2);
        itoa(snake.score, buff, 10);
        text_print(buff, 3, 3);

        press_any_to_continue(200);
        snake_init();
}

int main(void)
{
        init_peripherals();
        run_startup();
        run_menu();
        snake_init();

        while(1)
        {
                SLEEP(10);
                wait_for_next_input();

                if(snake.status == DEAD)
                {
                        run_end_screen();
                }

                update();
        }
}

void TIM3_IRQHandler(void)
{
        if(snake.status == ALIVE)
        {
                static int cicle = 0;
                if(cicle == 4 - snake.speed)
                {
                        queue_push();
                        snake.direction_set = FALSE;
                        cicle = 0;
                }
                else
                {
                        ++cicle;
                }
        }
        HAL_TIM_IRQHandler(&htim3);
}

void SystemClock_Config(void)
{
        RCC_OscInitTypeDef RCC_OscInitStruct;
        RCC_ClkInitTypeDef RCC_ClkInitStruct;

        /**Configure the main internal regulator output voltage
         */
        __HAL_RCC_PWR_CLK_ENABLE();

        __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE2);

        /**Initializes the CPU, AHB and APB busses clocks
         */
        RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
        RCC_OscInitStruct.HSIState = RCC_HSI_ON;
        RCC_OscInitStruct.HSICalibrationValue = 16;
        RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
        RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSI;
        RCC_OscInitStruct.PLL.PLLM = 16;
        RCC_OscInitStruct.PLL.PLLN = 336;
        RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV4;
        RCC_OscInitStruct.PLL.PLLQ = 7;
        if(HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
        {
                _Error_Handler(__FILE__, __LINE__);
        }

        /**Initializes the CPU, AHB and APB busses clocks
         */
        RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK |
                                      RCC_CLOCKTYPE_SYSCLK |
                                      RCC_CLOCKTYPE_PCLK1 | RCC_CLOCKTYPE_PCLK2;
        RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
        RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
        RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
        RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

        if(HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2) != HAL_OK)
        {
                _Error_Handler(__FILE__, __LINE__);
        }

        /**Configure the Systick interrupt time
         */
        HAL_SYSTICK_Config(HAL_RCC_GetHCLKFreq() / 1000);

        /**Configure the Systick
         */
        HAL_SYSTICK_CLKSourceConfig(SYSTICK_CLKSOURCE_HCLK);

        /* SysTick_IRQn interrupt configuration */
        HAL_NVIC_SetPriority(SysTick_IRQn, 0, 0);
}

/* SPI3 init function */
static void MX_SPI3_Init(void)
{
        /* SPI3 parameter configuration*/
        hspi3.Instance = SPI3;
        hspi3.Init.Mode = SPI_MODE_MASTER;
        hspi3.Init.Direction = SPI_DIRECTION_1LINE;
        hspi3.Init.DataSize = SPI_DATASIZE_8BIT;
        hspi3.Init.CLKPolarity = SPI_POLARITY_LOW;
        hspi3.Init.CLKPhase = SPI_PHASE_1EDGE;
        hspi3.Init.NSS = SPI_NSS_SOFT;
        hspi3.Init.BaudRatePrescaler = SPI_BAUDRATEPRESCALER_2;
        hspi3.Init.FirstBit = SPI_FIRSTBIT_MSB;
        hspi3.Init.TIMode = SPI_TIMODE_DISABLE;
        hspi3.Init.CRCCalculation = SPI_CRCCALCULATION_DISABLE;
        hspi3.Init.CRCPolynomial = 10;
        if(HAL_SPI_Init(&hspi3) != HAL_OK)
        {
                _Error_Handler(__FILE__, __LINE__);
        }
}

/* TIM3 init function */
static void MX_TIM3_Init(void)
{
        TIM_ClockConfigTypeDef sClockSourceConfig;
        TIM_MasterConfigTypeDef sMasterConfig;

        htim3.Instance = TIM3;
        htim3.Init.Prescaler = 47999;
        htim3.Init.CounterMode = TIM_COUNTERMODE_UP;
        htim3.Init.Period = 249;
        htim3.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
        if(HAL_TIM_Base_Init(&htim3) != HAL_OK)
        {
                _Error_Handler(__FILE__, __LINE__);
        }

        sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
        if(HAL_TIM_ConfigClockSource(&htim3, &sClockSourceConfig) != HAL_OK)
        {
                _Error_Handler(__FILE__, __LINE__);
        }

        sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
        sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
        if(HAL_TIMEx_MasterConfigSynchronization(&htim3, &sMasterConfig) !=
           HAL_OK)
        {
                _Error_Handler(__FILE__, __LINE__);
        }
}

/** Configure pins as
        * Analog
        * Input
        * Output
        * EVENT_OUT
        * EXTI
     PA2   ------> USART2_TX
     PA3   ------> USART2_RX
*/
static void MX_GPIO_Init(void)
{
        GPIO_InitTypeDef GPIO_InitStruct;

        /* GPIO Ports Clock Enable */
        __HAL_RCC_GPIOC_CLK_ENABLE();
        __HAL_RCC_GPIOH_CLK_ENABLE();
        __HAL_RCC_GPIOA_CLK_ENABLE();
        __HAL_RCC_GPIOB_CLK_ENABLE();
        __HAL_RCC_GPIOD_CLK_ENABLE();

        /*Configure GPIO pin Output Level */
        HAL_GPIO_WritePin(GPIOC, RST_Pin | CE_Pin | D9_Pin | DC_Pin,
                          GPIO_PIN_RESET);

        /*Configure GPIO pin Output Level */
        HAL_GPIO_WritePin(GPIOA, LD2_Pin | D7_Pin | D8_Pin, GPIO_PIN_RESET);

        /*Configure GPIO pin Output Level */
        HAL_GPIO_WritePin(D6_GPIO_Port, D6_Pin, GPIO_PIN_RESET);

        /*Configure GPIO pin : B1_Pin */
        GPIO_InitStruct.Pin = B1_Pin;
        GPIO_InitStruct.Mode = GPIO_MODE_IT_FALLING;
        GPIO_InitStruct.Pull = GPIO_NOPULL;
        HAL_GPIO_Init(B1_GPIO_Port, &GPIO_InitStruct);

        /*Configure GPIO pins : RST_Pin CE_Pin D9_Pin DC_Pin */
        GPIO_InitStruct.Pin = RST_Pin | CE_Pin | D9_Pin | DC_Pin;
        GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
        GPIO_InitStruct.Pull = GPIO_NOPULL;
        GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
        HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

        /*Configure GPIO pins : USART_TX_Pin USART_RX_Pin */
        GPIO_InitStruct.Pin = USART_TX_Pin | USART_RX_Pin;
        GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
        GPIO_InitStruct.Pull = GPIO_NOPULL;
        GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
        GPIO_InitStruct.Alternate = GPIO_AF7_USART2;
        HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

        /*Configure GPIO pins : LD2_Pin D7_Pin D8_Pin */
        GPIO_InitStruct.Pin = LD2_Pin | D7_Pin | D8_Pin;
        GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
        GPIO_InitStruct.Pull = GPIO_NOPULL;
        GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
        HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

        /*Configure GPIO pin : D6_Pin */
        GPIO_InitStruct.Pin = D6_Pin;
        GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
        GPIO_InitStruct.Pull = GPIO_NOPULL;
        GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
        HAL_GPIO_Init(D6_GPIO_Port, &GPIO_InitStruct);

        /*Configure GPIO pin : D2_Pin */
        GPIO_InitStruct.Pin = D2_Pin;
        GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
        GPIO_InitStruct.Pull = GPIO_PULLUP;
        HAL_GPIO_Init(D2_GPIO_Port, &GPIO_InitStruct);

        /*Configure GPIO pin : D5_Pin */
        GPIO_InitStruct.Pin = D5_Pin;
        GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
        GPIO_InitStruct.Pull = GPIO_PULLUP;
        HAL_GPIO_Init(D5_GPIO_Port, &GPIO_InitStruct);

        /*Configure GPIO pins : D3_Pin D4_Pin */
        GPIO_InitStruct.Pin = D3_Pin | D4_Pin;
        GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
        GPIO_InitStruct.Pull = GPIO_PULLUP;
        HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);
}

void _Error_Handler(char *file, int line)
{
        while(1)
        {
        }
}

#ifdef USE_FULL_ASSERT

void assert_failed(uint8_t *file, uint32_t line) {}
#endif /* USE_FULL_ASSERT */
